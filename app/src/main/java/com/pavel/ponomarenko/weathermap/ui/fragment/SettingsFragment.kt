package com.pavel.ponomarenko.weathermap.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.databinding.FragmentSettingsBinding
import com.pavel.ponomarenko.weathermap.mvvm.SettingsViewModel
import kotlinx.android.synthetic.main.fragment_settings.view.*


class SettingsFragment : Fragment() {

    private lateinit var myView: View
    private lateinit var viewModel: SettingsViewModel
    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myView = DataBindingUtil.inflate<FragmentSettingsBinding>(
            inflater, R.layout.fragment_settings, container, false
        )
            .also {
                binding = it
            }.root

        viewModel = ViewModelProviders.of(activity!!).get(SettingsViewModel::class.java)
        binding.viewModel = viewModel

        myView.celsiusFahrenheitTv.setOnClickListener {
            viewModel.setUnit()
        }

        return myView
    }
}
