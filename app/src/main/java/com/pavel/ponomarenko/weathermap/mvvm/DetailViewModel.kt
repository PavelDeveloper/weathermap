package com.pavel.ponomarenko.weathermap.mvvm

import android.annotation.SuppressLint
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.adapter.DaysListAdapter
import com.pavel.ponomarenko.weathermap.app.WeatherApplication
import com.pavel.ponomarenko.weathermap.data.PrefManager
import com.pavel.ponomarenko.weathermap.data.database.WeatherDao
import com.pavel.ponomarenko.weathermap.data.repository.Repository
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherDetail
import com.pavel.ponomarenko.weathermap.rest.WeatherApi
import com.pavel.ponomarenko.weathermap.utils.Constants
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.BASE_IMG_URL
import com.pavel.ponomarenko.weathermap.utils.NetManager
import com.pavel.ponomarenko.weathermap.utils.extentions.getDateDay
import com.pavel.ponomarenko.weathermap.utils.extentions.setUnits
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class DetailViewModel(private val weatherDao: WeatherDao?) : BaseViewModel() {

    @Inject
    lateinit var weatherApi: WeatherApi

    @Inject
    lateinit var prefManager: PrefManager

    private var unitsStr: String = ""

    private lateinit var subscription: Disposable

    val cityId: MutableLiveData<Int> = MutableLiveData()

    val daysListAdapter: DaysListAdapter = DaysListAdapter()

    private val weatherData: MutableLiveData<WeatherData> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()

    var imageUrl: ObservableField<String> = ObservableField()

    val humidity = ObservableField<String>()
    val pressure = ObservableField<String>()
    val cloudiness = ObservableField<String>()
    val wind = ObservableField<String>()
    val tempMax = ObservableField<String>()
    val tempMin = ObservableField<String>()
    val toolbarTitle = ObservableField<String>()
    val graphShow = ObservableField<LineGraphSeries<DataPoint>>()

    private val compositeDisposable = CompositeDisposable()
    private lateinit var repository: Repository

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    init {
        imageUrl.set("")
        graphShow.set(
            LineGraphSeries(
                arrayOf(
                    DataPoint(0.0, 0.0)
                )
            )
        )
    }

    fun loadWeatherData() {
        toolbarTitle.set("")
        repository = Repository(
            netManager = NetManager(WeatherApplication.weatherApplication),
            cityId = cityId.value!!,
            units = prefManager.getUnit() ?: Constants.UNITS_METRIC,
            weatherApi = weatherApi,
            weatherDao = weatherDao!!
        )
        compositeDisposable
            .add(repository
                .getRepositories()!!
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ list ->
                    weatherData.value = list
                    setCurrentWeatherData()
                    onDaysListSuccess()
                },{
                    onDownloadWeatherError()
                }))
    }

    private fun setCurrentWeatherData() {
        unitsStr = setUnits(unitsStr = prefManager.getUnit() ?: Constants.UNITS_CELSIUS)
        toolbarTitle.set("${weatherData.value?.main?.temp}$unitsStr  ${weatherData.value?.name} ")
        humidity.set("Humidity: ${weatherData.value?.main?.humidity} %")
        pressure.set("Pressure: ${weatherData.value?.main?.pressure} hpa")
        cloudiness.set("Cloudiness: ${weatherData.value?.weather?.get(0)?.description}")
        wind.set("Wind: ${weatherData.value?.wind?.speed} m/s")
        tempMax.set("Temperature Max: ${weatherData.value?.main?.temp_max}$unitsStr")
        tempMin.set("Temperature Min: ${weatherData.value?.main?.temp_min}$unitsStr")
        imageUrl.set("$BASE_IMG_URL${weatherData.value?.weather?.get(0)?.icon}.png")
        makeGraph(getDateDay(weatherData.value?.list?.get(0)?.dt_txt ?: "2019-05-28 00:00:00"))
    }

    private fun onDaysListSuccess() {
        weatherData.value?.list?.let {
            daysListAdapter.updateDaysList(it)
        }
    }

    private fun makeGraph(currentDay: String) {
        val dataPoint: ArrayList<DataPoint> = ArrayList()
        var i = 1.0
        weatherData.value?.list?.let {
            val date = Observable.fromIterable(weatherData.value?.list)
                .filter { date ->
                    date.dt_txt?.let { day ->
                        getDateDay(day)
                    }
                        .equals(currentDay)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dataPoint.add(
                        DataPoint(
                            i++,
                            it.main?.temp ?: 0.0
                        )
                    )
                }, {}, {
                    graphShow.set(LineGraphSeries(dataPoint.toTypedArray()))
                })
        }
    }

    fun setDay(weatherDetail: WeatherDetail) {
        makeGraph(getDateDay(weatherDetail.dt_txt ?: "01.01"))
    }

    @SuppressLint("CheckResult")
    fun storeCitiesInDb() {
        weatherData.value.let {
            Observable.fromCallable { weatherDao?.insertAll(it!!) }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    Timber.d("Inserted $weatherData to DB...")
                },{
                    onDownloadWeatherError()
                })
        }
    }

    @SuppressLint("CheckResult")
    fun deleteFromDb() {
        cityId.value?.let {
            Observable.fromCallable { weatherDao?.deleteById(it) }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    Timber.d("Deleted city from DB...")
                },{})
        }
    }

    private fun onDownloadWeatherError(){
        errorMessage.value = R.string.post_error
    }
}