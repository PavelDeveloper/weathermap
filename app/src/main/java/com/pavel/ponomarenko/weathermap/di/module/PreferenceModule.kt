package com.pavel.ponomarenko.weathermap.di.module

import android.content.Context
import android.content.SharedPreferences
import com.pavel.ponomarenko.weathermap.app.WeatherApplication
import com.pavel.ponomarenko.weathermap.data.PrefManager
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class PreferenceModule {

    private val application = WeatherApplication.weatherApplication

    @Singleton
    @Provides
    @Inject
    internal fun provideSharedPreferences(): SharedPreferences {
        return return application.getSharedPreferences(PrefManager.PREFERENCES_NAME_KEY, Context.MODE_PRIVATE)

    }
}