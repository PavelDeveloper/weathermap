package com.pavel.ponomarenko.weathermap.model.apiModel

data class Coord (val lat: Double?, val lon: Double?)
