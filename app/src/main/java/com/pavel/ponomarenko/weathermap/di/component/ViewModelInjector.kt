package com.pavel.ponomarenko.weathermap.di.component


import com.pavel.ponomarenko.weathermap.data.repository.RepoRemoteDataSource
import com.pavel.ponomarenko.weathermap.di.module.NetworkFileModule
import com.pavel.ponomarenko.weathermap.di.module.PreferenceModule
import com.pavel.ponomarenko.weathermap.mvvm.*
import dagger.Component
import javax.inject.Singleton

    @Singleton
    @Component(modules = [NetworkFileModule::class, PreferenceModule::class])
    interface ViewModelInjector {

        fun inject(startPageViewModel: StartPageViewModel)
        fun inject(cityChoiceViewModel: CityChoiceViewModel)
        fun inject(detailViewModel: DetailViewModel)
        fun inject(dayViewModel: DayViewModel)
        fun inject(settingsViewModel: SettingsViewModel)
        fun inject(repoRemoteDataSource: RepoRemoteDataSource)
        fun inject(favoriteCellViewModel: FavoriteCellViewModel)

        @Component.Builder
        interface Builder {
            fun build(): ViewModelInjector
            fun networkFileModule(networkModule: NetworkFileModule): Builder
            fun preferenceModule(preferenceModule: PreferenceModule): Builder
        }
    }