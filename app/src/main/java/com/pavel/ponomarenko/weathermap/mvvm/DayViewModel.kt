package com.pavel.ponomarenko.weathermap.mvvm

import android.annotation.SuppressLint
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.pavel.ponomarenko.weathermap.data.PrefManager
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherDetail
import com.pavel.ponomarenko.weathermap.utils.Constants
import com.pavel.ponomarenko.weathermap.utils.extentions.setUnits
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DayViewModel : BaseViewModel() {

    @Inject
    lateinit var prefManager: PrefManager

    private var unitsStr: String = ""

    private val temp = MutableLiveData<String>()
    private val wind = MutableLiveData<String>()
    private val humidity = MutableLiveData<String>()
    private val pressure = MutableLiveData<String>()
    private val date = MutableLiveData<String>()
    var imageUrl = ObservableField<String>()

    init {
        unitsStr = setUnits(unitsStr = prefManager.getUnit() ?: Constants.UNITS_CELSIUS)
    }

    fun bind(model: WeatherDetail) {
        imageUrl.set("${Constants.BASE_IMG_URL}${model.weather?.get(0)?.icon}.png")
        temp.value = "${model.main?.temp.toString()}$unitsStr"
        wind.value = "${model.wind?.speed.toString()}m/s"
        humidity.value = "${model.main?.humidity.toString()}%"
        pressure.value = "${model.main?.pressure.toString()}hpa"
        date.value = getDateFormat(model.dt_txt.toString())
    }

    fun getTemp(): MutableLiveData<String> {
        return temp
    }

    fun getWind(): MutableLiveData<String> {
        return wind
    }

    fun getHumidity(): MutableLiveData<String> {
        return humidity
    }

    fun getPressure(): MutableLiveData<String> {
        return pressure
    }

    fun getDate(): MutableLiveData<String> {
        return date
    }

    @SuppressLint("SimpleDateFormat")
        fun getDateFormat(data: String): String {
            val date: Date? = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data)
            return SimpleDateFormat("dd.MM HH:mm").format(date)
        }
}