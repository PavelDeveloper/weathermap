package com.pavel.ponomarenko.weathermap.data

import android.content.SharedPreferences
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.UNITS_METRIC
import javax.inject.Inject

class PrefManager @Inject constructor(private var sharedPreferences: SharedPreferences) {

    companion object {
        const val CITY_FILE_PATH_KEY = "city_path"
        const val IS_APP_STARTED_KEY = "is_started"
        const val PREFERENCES_NAME_KEY = "weather_key"
        const val PREFERENCES_UNIT_KEY = "unit_key"
    }

    fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    fun saveCityFilePath(path: String) {
        sharedPreferences.edit().putString(CITY_FILE_PATH_KEY, path).apply()
    }

    fun getCityPath(): String? = sharedPreferences.getString(CITY_FILE_PATH_KEY, "")

    fun saveStartFlagApp(flag: Boolean) {
        sharedPreferences.edit().putBoolean(IS_APP_STARTED_KEY, flag).apply()
    }

    fun getStartFlagApp(): Boolean? = sharedPreferences.getBoolean(IS_APP_STARTED_KEY, false)


    fun saveUnit(unit: String) {
        sharedPreferences.edit().putString(PREFERENCES_UNIT_KEY, unit).apply()
    }

    fun getUnit(): String? = sharedPreferences.getString(PREFERENCES_UNIT_KEY, UNITS_METRIC)
}

