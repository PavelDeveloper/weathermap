package com.pavel.ponomarenko.weathermap.utils.extentions

import android.annotation.SuppressLint
import android.content.ContextWrapper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.utils.Constants
import io.reactivex.functions.BiFunction
import java.text.SimpleDateFormat
import java.util.*

fun View.getParentActivity(): AppCompatActivity? {
    var context = this.context
    while (context is ContextWrapper) {
        if (context is AppCompatActivity) {
            return context
        }
        context = context.baseContext
    }
    return null
}

@SuppressLint("SimpleDateFormat")
fun getDate(date: String): Date {
    return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date)
}

@SuppressLint("SimpleDateFormat")
fun getDateDay(data: String): String {
    val date: Date? = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data)
    return SimpleDateFormat("dd.MM").format(date)
}

@SuppressLint("SimpleDateFormat")
fun getTimeDay(data: String): String {
    val date: Date? = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data)
    return SimpleDateFormat("HH:mm").format(date)
}

fun setUnits(unitsStr: String): String {
    when (unitsStr) {
        Constants.UNITS_IMPERIAL -> return Constants.UNITS_FAHRENHEIT
    }
    return Constants.UNITS_CELSIUS
}


val allWeatherData = BiFunction<WeatherData, WeatherData, WeatherData> { current, fiveDays ->
    current.list = fiveDays.list
    current
}
