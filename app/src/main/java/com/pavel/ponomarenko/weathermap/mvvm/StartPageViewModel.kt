package com.pavel.ponomarenko.weathermap.mvvm

import android.os.Environment
import androidx.lifecycle.MutableLiveData
import com.pavel.ponomarenko.weathermap.data.PrefManager
import com.pavel.ponomarenko.weathermap.rest.WeatherApi
import com.pavel.ponomarenko.weathermap.utils.Constants
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.BASE_FILE_URL
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import okio.BufferedSink
import okio.Okio
import retrofit2.Response
import timber.log.Timber
import java.io.File
import java.util.zip.GZIPInputStream
import javax.inject.Inject


@Suppress("UNREACHABLE_CODE")
class StartPageViewModel: BaseViewModel() {

    @Inject
    lateinit var weatherApi: WeatherApi

    @Inject
    lateinit var prefManager: PrefManager

    val loaded: MutableLiveData<Boolean> = MutableLiveData()

    private lateinit var subscription: Disposable

    private fun loadCityFile() {
        subscription = weatherApi.downloadFileByUrlRx(BASE_FILE_URL)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onDownloadCitiesFileStart() }
            .doOnTerminate { onDownloadCitiesFileFinish() }
            .flatMap { dataSourceObservable(response = it) }
            .subscribe(
                {
                    prefManager.saveCityFilePath(path = it.absolutePath)
                    prefManager.saveStartFlagApp(flag = true)
                    onDownloadCitiesFileSuccess()
                },
                {
                    onDownloadCitiesFileError()
                }, {
                    Timber.i("Error ADD FILE")
                }
            )
    }

    private fun dataSourceObservable(response: Response<ResponseBody>): Observable<File> {
        return Observable.create {
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absoluteFile,
                Constants.CITY_FILE_NAME
            )
            val sink: BufferedSink = Okio.buffer(Okio.sink(file))
            sink.write(GZIPInputStream(response.body()!!.source().inputStream()).readBytes())
            sink.close()
            it.onNext(file)
            it.onComplete()
        }
    }

    fun startDownloadCityFile() {
        loadCityFile()
    }

    private fun onDownloadCitiesFileStart() {
    }

    private fun onDownloadCitiesFileFinish() {

    }

    private fun onDownloadCitiesFileSuccess() {
        loaded.value = true
    }

    private fun onDownloadCitiesFileError() {

    }
}