package com.pavel.ponomarenko.weathermap.data.database

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.pavel.ponomarenko.weathermap.mvvm.DetailViewModel
import com.pavel.ponomarenko.weathermap.mvvm.FavoriteCitiesViewModel

class ViewModelFactory(private val activity: AppCompatActivity): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "dataBaseModel").build()
            @Suppress("UNCHECKED_CAST")
            return DetailViewModel(db.weatherDao()) as T
        }
        if (modelClass.isAssignableFrom(FavoriteCitiesViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "dataBaseModel").build()
            @Suppress("UNCHECKED_CAST")
            return FavoriteCitiesViewModel(db.weatherDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}
