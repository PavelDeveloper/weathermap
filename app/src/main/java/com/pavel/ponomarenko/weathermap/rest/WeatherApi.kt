package com.pavel.ponomarenko.weathermap.rest

import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.utils.Constants
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url


interface WeatherApi {

    @GET
    fun downloadFileByUrlRx(@Url url: String): Observable<Response<ResponseBody>>

    @GET("/data/2.5/forecast")
    fun getFiveDaysWeatherData(@Query("id") cityId: Int,
                               @Query("units") units: String,
                               @Query("APPID") api_key: String = Constants.API_KEY): Observable<WeatherData>

    @GET("data/2.5/weather")
    fun getCurrentWeatherData(@Query("id") cityId: Int,
                       @Query("units") units: String,
                       @Query("APPID") api_key: String = Constants.API_KEY): Observable<WeatherData>
}
