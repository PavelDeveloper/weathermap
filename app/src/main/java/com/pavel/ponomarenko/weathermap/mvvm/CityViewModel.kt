package com.pavel.ponomarenko.weathermap.mvvm

import androidx.lifecycle.MutableLiveData
import com.pavel.ponomarenko.weathermap.model.CityModel

class CityViewModel: BaseViewModel() {

    private val cityName = MutableLiveData<String>()
    private val countryName = MutableLiveData<String>()

    fun bind(model: CityModel){
        cityName.value = model.name
        countryName.value = model.country
    }

    fun getCityName():MutableLiveData<String>{
        return cityName
    }

    fun getCountryName():MutableLiveData<String>{
        return countryName
    }
}