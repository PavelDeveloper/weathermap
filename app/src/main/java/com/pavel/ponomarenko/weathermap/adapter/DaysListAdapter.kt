package com.pavel.ponomarenko.weathermap.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.databinding.CellWeatherDayBinding
import com.pavel.ponomarenko.weathermap.interfaces.IOnClickDayItem
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherDetail
import com.pavel.ponomarenko.weathermap.mvvm.DayViewModel

class DaysListAdapter : RecyclerView.Adapter<DaysListAdapter.ViewHolder>() {

    private lateinit var weatherDataList: ArrayList<WeatherDetail>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DaysListAdapter.ViewHolder {
        val binding: CellWeatherDayBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.cell_weather_day, parent, false)
        return ViewHolder(binding = binding)
    }

    private var listener: IOnClickDayItem? = null

    fun setClickListener(listener: IOnClickDayItem) {
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return if(::weatherDataList.isInitialized) weatherDataList.size else 0
    }

    override fun onBindViewHolder(holder: DaysListAdapter.ViewHolder, position: Int) {
        holder.bind(day = weatherDataList[position])
        holder.itemView.setOnClickListener{listener?.onClickDay(weatherDataList[position])}
    }

    fun updateDaysList(daysList: List<WeatherDetail>) {
        this.weatherDataList = daysList as ArrayList<WeatherDetail>
        notifyDataSetChanged()
    }

    fun clear() {
        weatherDataList.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: CellWeatherDayBinding) : RecyclerView.ViewHolder(binding.root) {

        private val dayViewModel = DayViewModel()

        fun bind(day: WeatherDetail) {
            dayViewModel.bind(model = day)
            binding.viewModel = dayViewModel
        }
    }
}