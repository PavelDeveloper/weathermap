package com.pavel.ponomarenko.weathermap.mvvm

import androidx.lifecycle.ViewModel
import com.pavel.ponomarenko.weathermap.di.component.DaggerViewModelInjector
import com.pavel.ponomarenko.weathermap.di.component.ViewModelInjector
import com.pavel.ponomarenko.weathermap.di.module.NetworkFileModule

abstract class BaseViewModel: ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkFileModule(NetworkFileModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is CityChoiceViewModel -> injector.inject(this)
            is StartPageViewModel -> injector.inject(this)
            is DetailViewModel -> injector.inject(this)
            is DayViewModel -> injector.inject(this)
            is SettingsViewModel -> injector.inject(this)
            is FavoriteCellViewModel -> injector.inject(this)
        }
    }
}