package com.pavel.ponomarenko.weathermap.utils

class Constants {
    companion object {
        const val BASE_URL: String = "http://api.openweathermap.org"
        const val BASE_FILE_URL: String = "http://bulk.openweathermap.org/sample/current.city.list.min.json.gz"
        const val BASE_IMG_URL: String = "https://openweathermap.org/img/w/"
        const val CITY_FILE_NAME: String = "cities.json"
        const val API_KEY: String = "824dbfa774dd77ac7eba82c514b9ca2d"
        const val UNITS_METRIC: String = "metric"
        const val UNITS_CELSIUS: String = "°C"
        const val UNITS_IMPERIAL: String = "imperial"
        const val UNITS_FAHRENHEIT: String = "°F"
    }
}