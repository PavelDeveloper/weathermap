package com.pavel.ponomarenko.weathermap.data.repository

import com.pavel.ponomarenko.weathermap.data.database.WeatherDao
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RepoLocalDataSource {

    fun getRepositories(weatherDao: WeatherDao, cityId: Int): Observable<WeatherData>? {
        return weatherDao.getCityById(cityId)
            .toObservable()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
            }
    }
}
