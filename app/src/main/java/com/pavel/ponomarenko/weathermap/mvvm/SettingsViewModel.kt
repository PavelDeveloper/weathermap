package com.pavel.ponomarenko.weathermap.mvvm

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.lifecycle.MutableLiveData
import com.pavel.ponomarenko.weathermap.data.PrefManager
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.UNITS_CELSIUS
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.UNITS_FAHRENHEIT
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.UNITS_IMPERIAL
import com.pavel.ponomarenko.weathermap.utils.Constants.Companion.UNITS_METRIC
import javax.inject.Inject


class SettingsViewModel : BaseViewModel() {

    @Inject
    lateinit var prefManager: PrefManager

    private val unitsText = "$UNITS_CELSIUS/$UNITS_FAHRENHEIT"
    private var spannable: SpannableString

    private var unit: MutableLiveData<SpannableString> = MutableLiveData()

    init {
        spannable = SpannableString(unitsText)
        installUnit()
    }

    fun setUnit() {
        when (prefManager.getUnit() ?: UNITS_METRIC) {
            UNITS_METRIC -> {
                prefManager.saveUnit(UNITS_IMPERIAL)
            }
            UNITS_IMPERIAL -> {
                prefManager.saveUnit(UNITS_METRIC)
            }
        }
        installUnit()
    }

    private fun installUnit() {
        when (prefManager.getUnit() ?: UNITS_METRIC) {
            UNITS_METRIC -> {
                spannable.setSpan(
                    ForegroundColorSpan(Color.RED),
                    0, 2,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                unit.value = spannable
                spannable.setSpan(
                    ForegroundColorSpan(Color.BLACK),
                    3, 5,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                unit.value = spannable
                prefManager.saveUnit(UNITS_METRIC)
            }
            UNITS_IMPERIAL -> {
                spannable.setSpan(
                    ForegroundColorSpan(Color.RED),
                    3, 5,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                unit.value = spannable
                spannable.setSpan(
                    ForegroundColorSpan(Color.BLACK),
                    0, 2,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                unit.value = spannable
                prefManager.saveUnit(UNITS_IMPERIAL)
            }
        }
    }

    fun getUnit(): MutableLiveData<SpannableString> {
        return unit
    }
}