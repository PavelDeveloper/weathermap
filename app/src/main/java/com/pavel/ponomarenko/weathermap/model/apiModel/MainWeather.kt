package com.pavel.ponomarenko.weathermap.model.apiModel


data class MainWeather(
    val temp: Double?,
    val temp_min: Double?,
    val temp_max: Double?,
    val pressure: Double?,
    val sea_level: Double?,
    val grnd_level: Double?,
    val humidity: Double?,
    val temp_kf: Double?
)