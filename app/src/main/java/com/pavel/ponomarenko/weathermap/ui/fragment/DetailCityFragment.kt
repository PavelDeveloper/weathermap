package com.pavel.ponomarenko.weathermap.ui.fragment


import android.os.Bundle
import android.view.*
import android.widget.LinearLayout.HORIZONTAL
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.data.database.ViewModelFactory
import com.pavel.ponomarenko.weathermap.databinding.FragmentDetailBinding
import com.pavel.ponomarenko.weathermap.interfaces.IOnBackPressed
import com.pavel.ponomarenko.weathermap.interfaces.IOnClickDayItem
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherDetail
import com.pavel.ponomarenko.weathermap.mvvm.DetailViewModel
import kotlinx.android.synthetic.main.fragment_detail.*


class DetailCityFragment : Fragment(), IOnClickDayItem, IOnBackPressed {

    private var cityId: Int? = null
    private lateinit var myView: View
    private lateinit var viewModel: DetailViewModel
    private lateinit var binding: FragmentDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, ViewModelFactory((activity as AppCompatActivity?)!!))
            .get(DetailViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null)
                Toast.makeText(context, R.string.post_error, Toast.LENGTH_LONG).show()
        })

        arguments?.let {
            val args = DetailCityFragmentArgs.fromBundle(it)
            cityId = args.cityId
            viewModel.cityId.value = cityId
            viewModel.loadWeatherData()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myView = DataBindingUtil.inflate<FragmentDetailBinding>(
            inflater, R.layout.fragment_detail, container, false
        )
            .also {
                binding = it
            }.root
        setHasOptionsMenu(true)
        binding.viewModel = viewModel
        return myView
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.daysRecycler.apply {
            layoutManager = LinearLayoutManager(myView.context, RecyclerView.HORIZONTAL, false)
            addItemDecoration(DividerItemDecoration(activity, HORIZONTAL))
        }
        viewModel.daysListAdapter.setClickListener(this)
    }

    override fun onClickDay(weatherDetail: WeatherDetail) {
        binding.graph.removeAllSeries()
        viewModel.setDay(weatherDetail = weatherDetail)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.detail_city_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.app_bar_star -> {
                item.setIcon(R.drawable.star)
                viewModel.storeCitiesInDb()
            }
            R.id.app_bar_delete -> {
                item.isVisible = false
                viewModel.deleteFromDb()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).setSupportActionBar(toolbarWeather)
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar!!.show()
    }

    override fun onBackPressed(): Boolean {
        return true
    }
}
