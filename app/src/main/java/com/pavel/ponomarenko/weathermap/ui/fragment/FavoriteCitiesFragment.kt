package com.pavel.ponomarenko.weathermap.ui.fragment


import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.data.database.ViewModelFactory
import com.pavel.ponomarenko.weathermap.databinding.FragmentFavoriteCitiesBinding
import com.pavel.ponomarenko.weathermap.interfaces.IOnBackPressed
import com.pavel.ponomarenko.weathermap.interfaces.IOnClickFavCityItem
import com.pavel.ponomarenko.weathermap.mvvm.FavoriteCitiesViewModel
import kotlinx.android.synthetic.main.fragment_favorite_cities.*
import kotlinx.android.synthetic.main.fragment_favorite_cities.view.*

class FavoriteCitiesFragment : Fragment(), IOnClickFavCityItem, IOnBackPressed {


    private lateinit var myView: View
    private lateinit var viewModel: FavoriteCitiesViewModel
    private lateinit var binding: FragmentFavoriteCitiesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myView = DataBindingUtil.inflate<FragmentFavoriteCitiesBinding>(
            inflater, R.layout.fragment_favorite_cities, container, false
        )
            .also {
                binding = it
            }.root
        setHasOptionsMenu(true)
        myView.fab.setOnClickListener {
            activity!!.findNavController(R.id.nav_host).navigate(R.id.choice_city_dest)
        }
        return myView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProviders.of(activity!!).get(FavoriteCitiesViewModel::class.java)
        viewModel = ViewModelProviders.of(this, ViewModelFactory((activity as AppCompatActivity?)!!)).get(
            FavoriteCitiesViewModel::class.java)
        binding.favoriteRV.apply {
            layoutManager = LinearLayoutManager(myView.context, RecyclerView.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(activity, LinearLayout.VERTICAL))
        }
        binding.viewModel = viewModel
        viewModel.cityListAdapter.setClickListener(this)
    }

    override fun onClickCity(cityId: Int) {
        val action = DetailCityFragmentDirections.actionGlobalParamsDest()
        action.cityId = cityId
        activity!!.findNavController(R.id.nav_host).navigate(action)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.bottomappbar_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.app_bar_fav -> {
                activity!!.findNavController(R.id.nav_host).navigate(R.id.settings_dest)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).setSupportActionBar(bottom_app_bar)
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar!!.show()
    }

    override fun onBackPressed(): Boolean {
        return true
    }
}
