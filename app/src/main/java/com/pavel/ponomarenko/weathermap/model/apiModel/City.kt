package com.pavel.ponomarenko.weathermap.model.apiModel


data class City (val id: Int?, val name: String?,  val coord: Coord?, val country: String?)