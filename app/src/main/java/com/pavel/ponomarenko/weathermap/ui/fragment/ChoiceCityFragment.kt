package com.pavel.ponomarenko.weathermap.ui.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.databinding.FragmentChoiceCityBinding
import com.pavel.ponomarenko.weathermap.interfaces.IOnClickCityItem
import com.pavel.ponomarenko.weathermap.model.CityModel
import com.pavel.ponomarenko.weathermap.mvvm.CityChoiceViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_choice_city.view.*
import java.util.concurrent.TimeUnit

class ChoiceCityFragment : Fragment(), IOnClickCityItem {

    private lateinit var myView: View
    private lateinit var viewModel: CityChoiceViewModel
    private lateinit var binding: FragmentChoiceCityBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myView = DataBindingUtil.inflate<FragmentChoiceCityBinding>(
            inflater, R.layout.fragment_choice_city, container, false
        )
            .also {
                binding = it
            }.root
        myView.button.setOnClickListener {
            if (viewModel.getCityId().value != null && viewModel.getCityId().value!! > 0) {
                val action = DetailCityFragmentDirections.actionGlobalParamsDest()
                action.cityId = viewModel.getCityId().value!!
                activity!!.findNavController(R.id.nav_host).navigate(action)
            } else {
                Toast.makeText(myView.context, "Chose a city from the list.", Toast.LENGTH_LONG).show()
            }

        }
        return myView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(CityChoiceViewModel::class.java)
        binding.cityRecyclerView.apply {
            layoutManager = LinearLayoutManager(myView.context, RecyclerView.VERTICAL, false)
        }
        binding.viewModel = viewModel
        viewModel.cityListAdapter.setClickListener(this)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
          RxTextView.textChanges(myView.cityName)
              .debounce(300, TimeUnit.MILLISECONDS)
              .skip(2)
              .filter { it.toString().isNotEmpty() }
              .subscribeOn(Schedulers.newThread())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe({
                  viewModel.cityListAdapter.filter(it)
              }, {
              })
    }

    override fun onClickCity(cityModel: CityModel) {
        viewModel.setCityName(cityModel = cityModel)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.hide()
    }
}