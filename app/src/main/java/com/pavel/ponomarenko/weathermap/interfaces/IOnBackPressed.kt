package com.pavel.ponomarenko.weathermap.interfaces

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}