package com.pavel.ponomarenko.weathermap.model.apiModel

data class WeatherDetail(val dt: Int?,
                    val main: MainWeather?,
                    val weather: List<Weather>?,
                    val clouds: Clouds?,
                    val wind: Wind?,
                    val snow: Snow?,
                    val sys: Sys?,
                    val dt_txt: String?)
