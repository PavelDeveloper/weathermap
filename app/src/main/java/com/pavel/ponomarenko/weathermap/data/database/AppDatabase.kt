package com.pavel.ponomarenko.weathermap.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData

@Database(entities = [WeatherData::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
}
