package com.pavel.ponomarenko.weathermap.model.apiModel

data class Sys(val pod: String?, val message: Float, val country: String?, val sunrise: Int?, val sunset: Int?)