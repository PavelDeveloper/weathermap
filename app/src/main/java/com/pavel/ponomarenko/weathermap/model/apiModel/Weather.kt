package com.pavel.ponomarenko.weathermap.model.apiModel

data class Weather (
    val id: Int?,
    val main: String?,
    val description: String?,
    val icon: String?)