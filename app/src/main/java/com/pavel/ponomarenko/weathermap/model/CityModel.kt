package com.pavel.ponomarenko.weathermap.model

data class CityModel(val name: String?, val country: String?, val id: Int?)