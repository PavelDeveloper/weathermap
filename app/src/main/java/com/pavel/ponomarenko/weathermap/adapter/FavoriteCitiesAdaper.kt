package com.pavel.ponomarenko.weathermap.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.databinding.CellFavoriteCitiesBinding
import com.pavel.ponomarenko.weathermap.interfaces.IOnClickFavCityItem
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.mvvm.FavoriteCellViewModel

class FavoriteCitiesAdaper  : RecyclerView.Adapter<FavoriteCitiesAdaper.ViewHolder>() {

    private lateinit var favoriteCitiesList: ArrayList<WeatherData>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteCitiesAdaper.ViewHolder {
        val binding: CellFavoriteCitiesBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.cell_favorite_cities, parent, false)
        return ViewHolder(binding = binding)
    }

    private var listener: IOnClickFavCityItem? = null

    fun setClickListener(listener: IOnClickFavCityItem) {
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return if(::favoriteCitiesList.isInitialized) favoriteCitiesList.size else 0
    }

    override fun onBindViewHolder(holder: FavoriteCitiesAdaper.ViewHolder, position: Int) {
        holder.bind(city = favoriteCitiesList[position])
        holder.itemView.setOnClickListener{listener?.onClickCity(favoriteCitiesList[position].id)}
    }

    fun updateCitiesList(daysList: List<WeatherData>) {
        this.favoriteCitiesList = daysList as ArrayList<WeatherData>
        notifyDataSetChanged()
    }

    fun clear() {
        favoriteCitiesList.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: CellFavoriteCitiesBinding) : RecyclerView.ViewHolder(binding.root) {

        private val favCityViewModel = FavoriteCellViewModel()

        fun bind(city: WeatherData) {
            favCityViewModel.bind(model = city)
            binding.viewModel = favCityViewModel
        }
    }
}