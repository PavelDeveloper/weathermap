package com.pavel.ponomarenko.weathermap.data.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pavel.ponomarenko.weathermap.model.apiModel.*

class Converters {

    companion object {

        @TypeConverter
        @JvmStatic
        fun fromOptionWeatherDetailList(weatherDetailValues: List<WeatherDetail>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<WeatherDetail>>(){}.type
            return gson.toJson(weatherDetailValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionWeatherDetailList(optionValuesString: String): List<WeatherDetail>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<WeatherDetail>>(){}.type
            return gson.fromJson(optionValuesString, listType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionCity(city: City?): String? {
            val gson = Gson()
            val cityType = object : TypeToken<City>(){}.type
            return gson.toJson(city, cityType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionCity(city: String): City? {
            val gson = Gson()
            val listType = object : TypeToken<City>(){}.type
            return gson.fromJson(city, listType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionWeatherList(weatherValuesList: List<Weather>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<Weather>>(){}.type
            return gson.toJson(weatherValuesList, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionWeatherList(optionWeatherString: String): List<Weather>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<Weather>>(){}.type
            return gson.fromJson(optionWeatherString, listType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionMainWeather(mainWeather: MainWeather?): String? {
            val gson = Gson()
            val mainWeatherType = object : TypeToken<MainWeather>(){}.type
            return gson.toJson(mainWeather, mainWeatherType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionMainWeather(mainWeather: String): MainWeather? {
            val gson = Gson()
            val mainWeatherType = object : TypeToken<MainWeather>(){}.type
            return gson.fromJson(mainWeather, mainWeatherType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionWind(wind: Wind?): String? {
            val gson = Gson()
            val windType = object : TypeToken<Wind>(){}.type
            return gson.toJson(wind, windType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionWind(wind: String): Wind? {
            val gson = Gson()
            val windType = object : TypeToken<Wind>(){}.type
            return gson.fromJson(wind, windType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionClouds(clouds: Clouds?): String? {
            val gson = Gson()
            val windType = object : TypeToken<Clouds>(){}.type
            return gson.toJson(clouds, windType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionClouds(clouds: String): Clouds? {
            val gson = Gson()
            val cloudsType = object : TypeToken<Clouds>(){}.type
            return gson.fromJson(clouds, cloudsType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionSys(sys: Sys?): String? {
            val gson = Gson()
            val sysType = object : TypeToken<Sys>(){}.type
            return gson.toJson(sys, sysType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionSys(sys: String): Sys? {
            val gson = Gson()
            val sysType = object : TypeToken<Sys>(){}.type
            return gson.fromJson(sys, sysType)
        }
    }
}

