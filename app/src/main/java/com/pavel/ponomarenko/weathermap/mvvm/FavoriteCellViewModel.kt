package com.pavel.ponomarenko.weathermap.mvvm

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.pavel.ponomarenko.weathermap.data.PrefManager
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.utils.Constants
import com.pavel.ponomarenko.weathermap.utils.extentions.setUnits
import javax.inject.Inject

class FavoriteCellViewModel : BaseViewModel() {

    @Inject
    lateinit var prefManager: PrefManager

    private var unitsStr: String = ""

    private val favCityName = MutableLiveData<String>()
    private val favCityTemp = MutableLiveData<String>()
    var favCityImageUrl = ObservableField<String>()

    init {
        unitsStr = setUnits(unitsStr = prefManager.getUnit() ?: Constants.UNITS_CELSIUS)
    }

    fun bind(model: WeatherData) {
        favCityImageUrl.set("${Constants.BASE_IMG_URL}${model.weather?.get(0)?.icon}.png")
        favCityTemp.value = "${model.main?.temp}$unitsStr"
        favCityName.value = model.name
    }

    fun getFavCityTemp(): MutableLiveData<String> {
        return favCityTemp
    }

    fun getFavCityName(): MutableLiveData<String> {
        return favCityName
    }
}