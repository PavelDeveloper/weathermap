package com.pavel.ponomarenko.weathermap.mvvm

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.pavel.ponomarenko.weathermap.adapter.FavoriteCitiesAdaper
import com.pavel.ponomarenko.weathermap.data.database.WeatherDao
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class FavoriteCitiesViewModel(private val weatherDao: WeatherDao) : BaseViewModel() {

    val cityListAdapter: FavoriteCitiesAdaper = FavoriteCitiesAdaper()

    private val citiesWeatherData: MutableLiveData<List<WeatherData>> = MutableLiveData()

    init {
        getUsersFromDb()!!
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                citiesWeatherData.value = it
                onCitiesListSuccess()
                Log.i("TAG", "getUsersFromDb it is: ${it.get(0).name}")
            }, {
                Log.i("TAG", "getUsersFromDb it is error: ${it.localizedMessage}")
            })
    }

    private fun getUsersFromDb(): Observable<List<WeatherData>>? {
        return weatherDao.all().filter { it.isNotEmpty() }
            .toObservable()
            .doOnNext {
                Timber.d("Dispatching ${it.size} users from DB...")
            }
    }

    private fun onCitiesListSuccess() {
        cityListAdapter.updateCitiesList(citiesWeatherData.value!!)
    }
}