package com.pavel.ponomarenko.weathermap.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.databinding.CellCitiesBinding
import com.pavel.ponomarenko.weathermap.interfaces.IOnClickCityItem
import com.pavel.ponomarenko.weathermap.model.CityModel
import com.pavel.ponomarenko.weathermap.mvvm.CityViewModel

class CityListAdapter : RecyclerView.Adapter<CityListAdapter.ViewHolder>() {

    private  var cityList: ArrayList<CityModel> = ArrayList()
    private var sourceList:  ArrayList<CityModel> = ArrayList()

    private var listener: IOnClickCityItem? = null

    fun setClickListener(listener: IOnClickCityItem) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityListAdapter.ViewHolder {
        val binding: CellCitiesBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.cell_cities, parent, false)
        return ViewHolder(binding = binding)
    }

    override fun getItemCount(): Int {
        return  cityList.size
    }

    override fun onBindViewHolder(holder: CityListAdapter.ViewHolder, position: Int) {
        holder.bind(city = cityList[position])
        holder.itemView.setOnClickListener{listener?.onClickCity(cityList[position])}
    }

    fun updateCityList(cityList: List<CityModel>) {
        sourceList.clear()
        sourceList.addAll(cityList)
    }

    fun clear() {
        sourceList.clear()
        notifyDataSetChanged()
    }

    fun filter(query: CharSequence) {
        cityList.clear()
        sourceList.forEach{
            if (it.name?.contains(query, ignoreCase = true)!!) {
                cityList.add(it)
            }
        }
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: CellCitiesBinding) : RecyclerView.ViewHolder(binding.root) {

        private val cityViewModel = CityViewModel()

        fun bind(city: CityModel) {
            cityViewModel.bind(model = city)
            binding.viewModel = cityViewModel
        }
    }
}