package com.pavel.ponomarenko.weathermap.data.database

import androidx.room.*
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import io.reactivex.Single


@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg model: WeatherData)

    @Query("SELECT * FROM cities")
    fun all(): Single<List<WeatherData>>

    @Query("SELECT * FROM cities WHERE id = :id")
    fun getCityById(id: Int): Single<WeatherData>

    @Query("DELETE FROM cities WHERE id = :id")
    fun deleteById(id: Int)

    @Delete
    fun reallyDeleteWeatherData(data: WeatherData)
}
