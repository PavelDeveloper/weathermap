package com.pavel.ponomarenko.weathermap.data.repository

import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.rest.WeatherApi
import com.pavel.ponomarenko.weathermap.utils.extentions.allWeatherData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class RepoRemoteDataSource(val cityId: Int, private val units: String, private val weatherApi: WeatherApi) {

    fun getRepositories(): Observable<WeatherData> {
        return weatherApi.getCurrentWeatherData(cityId, units)
            .zipWith(weatherApi.getFiveDaysWeatherData(cityId, units), allWeatherData)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

