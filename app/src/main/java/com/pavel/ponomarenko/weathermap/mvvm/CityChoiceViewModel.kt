package com.pavel.ponomarenko.weathermap.mvvm

import android.annotation.SuppressLint
import android.os.Environment
import androidx.lifecycle.MutableLiveData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.adapter.CityListAdapter
import com.pavel.ponomarenko.weathermap.model.CityModel
import com.pavel.ponomarenko.weathermap.utils.Constants
import io.reactivex.Observable
import java.io.File
import java.io.FileInputStream
import java.nio.channels.FileChannel
import java.nio.charset.Charset


class CityChoiceViewModel : BaseViewModel() {

    private val cityName = MutableLiveData<String>()
    private val errorMessage: MutableLiveData<Int> = MutableLiveData()
    private val cityId: MutableLiveData<Int> = MutableLiveData()

    val cityListAdapter: CityListAdapter = CityListAdapter()

    init {
        parseJson()
    }

    @SuppressLint("CheckResult")
    private fun parseJson() {
        dataSourceObservable()
            .subscribe ({
                onCityListSuccess(it)
            }, {
                onCityListError()
        })
    }

    private fun readFile(): String {
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absoluteFile,
            Constants.CITY_FILE_NAME
        )
        val stream = FileInputStream(file)
        var jsonString = ""
        stream.use {
            val fileChannel = it.channel
            val mappedBiteBuffer = fileChannel.map(
                FileChannel.MapMode.READ_ONLY, 0, fileChannel.size()
            )
            jsonString = Charset.defaultCharset().decode(mappedBiteBuffer).toString()
        }
        return jsonString
    }

    private fun dataSourceObservable(): Observable<MutableList<CityModel>> {
        return Observable.create {
            val gson = GsonBuilder().setPrettyPrinting().create()
            val fileStr = readFile()
            val responseObject: MutableList<CityModel> =
                gson.fromJson(fileStr, object : TypeToken<List<CityModel>>() {}.type)
            it.onNext(responseObject)
            it.onComplete()
        }
    }

    private fun onCityListSuccess(cityList:List<CityModel>){
        cityListAdapter.updateCityList(cityList)
    }

    private fun onCityListError(){
        errorMessage.value = R.string.post_error
    }

    fun setCityName(cityModel: CityModel):MutableLiveData<String>{
        cityName.value = "${cityModel.name} ${cityModel.country}"
        cityId.value = cityModel.id
        return cityName
    }
    fun getCityName():MutableLiveData<String>{
        return cityName
    }

    fun getCityId():MutableLiveData<Int>{
        return cityId
    }
}