package com.pavel.ponomarenko.weathermap.interfaces

import com.pavel.ponomarenko.weathermap.model.CityModel
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherDetail

interface IOnClickCityItem {
        fun onClickCity(cityModel: CityModel)
}

interface IOnClickFavCityItem {
        fun onClickCity(cityId: Int)
}

interface IOnClickDayItem {
        fun onClickDay(weatherDetail: WeatherDetail)
}