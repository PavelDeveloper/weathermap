package com.pavel.ponomarenko.weathermap.app

import android.app.Application

class WeatherApplication: Application() {


    companion object{
        lateinit var weatherApplication: WeatherApplication
    }

    override fun onCreate() {
        super.onCreate()
        weatherApplication = this
    }
}