package com.pavel.ponomarenko.weathermap.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.databinding.FragmentStartPageBinding
import com.pavel.ponomarenko.weathermap.mvvm.StartPageViewModel

class StartPageFragment : Fragment() {

    private lateinit var viewModel: StartPageViewModel
    private lateinit var binding: FragmentStartPageBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        DataBindingUtil.inflate<FragmentStartPageBinding>(
            inflater,
            R.layout.fragment_start_page,
            container,
            false
        ).also {
            binding = it
        }.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(StartPageViewModel::class.java)

        if (viewModel.prefManager.getStartFlagApp()!! && viewModel.prefManager.getCityPath()!! != "") {
            activity!!.findNavController(R.id.nav_host).navigate(R.id.favorite_dest)
        } else {
            viewModel.startDownloadCityFile()
            viewModel.loaded.observe(this, Observer {
                activity!!.findNavController(R.id.nav_host).navigate(R.id.favorite_dest)
            })
        }
    }
}
