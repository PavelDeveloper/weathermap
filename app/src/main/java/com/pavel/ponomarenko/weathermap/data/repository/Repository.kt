package com.pavel.ponomarenko.weathermap.data.repository

import com.pavel.ponomarenko.weathermap.data.database.WeatherDao
import com.pavel.ponomarenko.weathermap.model.apiModel.WeatherData
import com.pavel.ponomarenko.weathermap.rest.WeatherApi
import com.pavel.ponomarenko.weathermap.utils.NetManager
import io.reactivex.Observable

class Repository (val netManager: NetManager, val cityId: Int, units: String, weatherApi: WeatherApi,
                  private val weatherDao: WeatherDao) {

    private val localDataSource = RepoLocalDataSource()
    private val remoteDataSource = RepoRemoteDataSource(cityId = cityId, units = units, weatherApi = weatherApi)

    fun getRepositories(): Observable<WeatherData>? {
        netManager.isConnectedToInternet?.let {
            if (it) {
                 return remoteDataSource.getRepositories()
            }
        }
        return localDataSource.getRepositories(weatherDao = weatherDao, cityId = cityId)
    }
}