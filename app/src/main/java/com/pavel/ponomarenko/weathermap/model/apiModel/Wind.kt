package com.pavel.ponomarenko.weathermap.model.apiModel

data class Wind (val speed: Double?,val deg: Double?)
