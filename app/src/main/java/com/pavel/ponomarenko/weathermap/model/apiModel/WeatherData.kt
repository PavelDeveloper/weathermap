package com.pavel.ponomarenko.weathermap.model.apiModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cities")
data class WeatherData (
    @field:PrimaryKey
    val id: Int,
    val message: Double?,
    val cnt: Double?,
    var list: List<WeatherDetail>?,
    val city: City?,
    var weather: List<Weather>?,
    var base: String?,
    var main: MainWeather?,
    var wind: Wind?,
    var clouds: Clouds?,
    var dt: Double?,
    var sys: Sys?,
    var name: String?)
