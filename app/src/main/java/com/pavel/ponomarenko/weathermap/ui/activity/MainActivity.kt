package com.pavel.ponomarenko.weathermap.ui.activity

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.pavel.ponomarenko.weathermap.R
import com.pavel.ponomarenko.weathermap.interfaces.IOnBackPressed
import com.pavel.ponomarenko.weathermap.ui.fragment.DetailCityFragment




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE)

        Permissions.check(this, permissions, null, null, object : PermissionHandler() {
            override fun onGranted() {
                findNavController(R.id.nav_host).navigate(R.id.startPageFragment)
            }
        })
    }

    override fun onBackPressed() {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host)
        val fragment = navHostFragment?.childFragmentManager?.fragments?.get(0)
        (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
            if (fragment is DetailCityFragment) {
                findNavController(R.id.nav_host).popBackStack (R.id.favorite_dest, true)
            } else {
                finish()
            }
        }
        super.onBackPressed()
    }
}
