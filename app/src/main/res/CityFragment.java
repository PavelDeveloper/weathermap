import com.pavel.ponomarenko.weathermap.R;
import com.pavel.ponomarenko.weathermap.BR;
import com.stfalcon.androidmvvmhelper.mvvm.fragments.BindingFragment;
import com.pavel.ponomarenko.weathermap.databinding.FragmentCityBinding;


public class CityFragment extends BindingFragment<CityFragmentVM, FragmentCityBinding> {

    public CityFragment() {
        // Required empty public constructor
    }

    public static CityFragment newInstance() {
        return new CityFragment();
    }

    @Override
    protected CityFragmentVM onCreateViewModel(FragmentCityBinding binding) {
        return new CityFragmentVM(this);
    }

    @Override
    public int getVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_city;
    }

}